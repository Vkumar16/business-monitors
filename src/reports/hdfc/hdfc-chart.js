import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import Data from './hdfc.controller'

const TotalReceived = Data.reduce((accum,{TotalReceive}) => {
    return [...accum, TotalReceive]
},[])

const TotalProcessed = Data.reduce((accum,{TotalProcessed}) => {
    return [...accum, TotalProcessed]
},[])

const TotalSuccess = Data.reduce((accum,{Success}) => {
    return [...accum, Success]
},[])

const TotalFailed = Data.reduce((accum,{Failed}) => {
    return [...accum, Failed]
},[])

const Time  = Data.reduce((accum,{Time}) => {
    return [...accum, Time]
},[])

const options = {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Single Date Wise'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: Time,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'No. Of Process'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    // plotOptions: {
    //     column: {
    //         pointPadding: 0.2,
    //         borderWidth: 0
    //     }
    // },
    series: [{
        name: 'Total Receive',
        data: TotalReceived

    }, {
        name: 'Total Processed',
        data: TotalProcessed
    }, {
        name: 'Success',
        data: TotalSuccess

    }, {
        name: 'Failed',
        data: TotalFailed

    }]
}
/* Date: '2022-01-01',
        Time: '10:00',
        TotalReceive: 120,
        TotalProcessed: 100,
        Success: 90,
        Failed: 10 */

const HdfcChart = () => {
    console.log("hdfc chart call")
    return (
        <HighchartsReact
            highcharts={Highcharts}
            options={options}
        />
    )

}

export default HdfcChart
