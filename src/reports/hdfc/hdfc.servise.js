import React, { useState, useEffect } from 'react'
import Data from './hdfc.controller'
import { Table } from 'react-bootstrap';
import HdfcChart from './hdfc-chart';

const HDFC = () => {
    const [leadPushData, setLeadPushData] = useState([])

    const setValue = () => {
        setLeadPushData(Data)
    }
    useEffect(() => {
        setValue()
    }, [])


    return (
        <div>
            <div>
                <HdfcChart />
            </div>
            <div>
                <h2>Lead Push Status Report</h2>
            </div>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Total Received</th>
                        <th>Total Processed</th>
                        <th>Success</th>
                        <th>Failed</th>
                    </tr>
                </thead>
                <tbody>
                    {leadPushData.length > 0 ?
                        leadPushData.map((eachData, index) =>
                            <tr key = {index}>
                                <td>{index + 1}</td>
                                <td>{eachData.Date}</td>
                                <td>{eachData.Time}</td>
                                <td>{eachData.TotalReceive}</td>
                                <td>{eachData.TotalProcessed}</td>
                                <td>{eachData.Success}</td>
                                <td>{eachData.Failed}</td>
                            </tr>
                        )
                        : null

                    }

                </tbody>
            </Table>

        </div>
    )

}

export default HDFC