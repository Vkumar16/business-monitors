import logo from './logo.svg';
import './App.css';
import HDFC from './reports/hdfc/hdfc.servise'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap'

function App() {
  return (
    <Container >
      <HDFC />
    </Container>

  );
}

export default App;
