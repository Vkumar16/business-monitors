
const data = [
    {
        Date: '2022-01-01',
        Time: '9:00',
        TotalReceive: 100,
        TotalProcessed: 98,
        Success: 90,
        Failed: 8
    },
    {
        Date: '2022-01-01',
        Time: '10:00',
        TotalReceive: 120,
        TotalProcessed: 100,
        Success: 90,
        Failed: 10
    },
    {
        Date: '2022-01-01',
        Time: '11:00',
        TotalReceive: 200,
        TotalProcessed: 180,
        Success: 100,
        Failed: 80
    },
    {
        Date: '2022-01-01',
        Time: '12:00',
        TotalReceive: 90,
        TotalProcessed: 90,
        Success: 90,
        Failed: 0
    },
    {
        Date: '2022-01-01',
        Time: '13:00',
        TotalReceive: 600,
        TotalProcessed: 300,
        Success: 267,
        Failed: 33
    },
    {
        Date: '2022-01-01',
        Time: '14:00',
        TotalReceive: 100,
        TotalProcessed: 98,
        Success: 90,
        Failed: 8
    },{
        Date: '2022-01-01',
        Time: '15:00',
        TotalReceive: 1000,
        TotalProcessed: 670,
        Success: 330,
        Failed: 340
    },{
        Date: '2022-01-01',
        Time: '16:00',
        TotalReceive: 100,
        TotalProcessed: 98,
        Success: 90,
        Failed: 8
    },
]

export default data